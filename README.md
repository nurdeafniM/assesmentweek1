**Table Of Content**
1. What is the approach ?
2. Why is the approach followed?
3. Problem faced while using the approach
4. Step to run

**What Is The Approach Followed**
* first step: Try to understand the completion steps in the game https://david-peter.de/cube-composer/
* second step: create a file containing the function for the solution that is in game 0.1 with the file name (migth.js) by exporting to the file to be created to test the codification created (cubeComposerJest.test.js)
* second step: create a level 1 path by exporting to the migth.js file with the file name (cubeComposer.js)
* third step: create a file to test the function that has been created with the file name (cubeComposerJest.test.js)
* step four: Add a function that is not at level 0.1 but is at level 1.1 in the file (migth.js) by exporting to file (cubeComposerJest.test.js)
*  step five: re-test the code that has been made for level 0.1 in the file

**Problem Faced While Using The Approach**
1. Confused to start creating functions
2. Incorrect variable call
3. Forget to npm init when you want to test the coding so that it error

**Step To Run**
1. Open the folder with the file that we want to test
2. Type **Jest** in the terminals and **enter**